﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace NubOS.Windows.Components
{
    public class Styling
    {
        public Color TitlebarColor;
        public Color TitlebarTextColor;

        public Color WindowBackground;
        public Color TextColor;

        public Color ButtonBackground;
        public Color ButtonText;

        public Styling(Color TitlebarColor, Color TitlebarTextColor,
            Color WindowBackground, Color TextColor,
            Color ButtonBackground, Color ButtonText)
        {
            this.TitlebarColor = TitlebarColor;
            this.TitlebarTextColor = TitlebarTextColor;
            this.WindowBackground = WindowBackground;
            this.TextColor = TextColor;
            this.ButtonBackground = ButtonBackground;
            this.ButtonText = ButtonText;
        }

    }

    public class DefaultStyles
    {
        public static Styling Light = new Styling(Color.DarkGray, Color.Black,
            Color.FromArgb(230, 230, 230), Color.Black, Color.FromArgb(190, 190, 190),
            Color.Black);
    }
}
