﻿using NubOS.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Console = Colorful.Console;

namespace NubOS.Windows.Components
{
    public class Button : Component
    {
        public event OnButtonPress OnPress;
        public delegate void OnButtonPress(ButtonPressArgs args);

        public string Text;
        
        public Button()
        {
            OnPress += new OnButtonPress((args) =>
            {
                Text = "Suk me kok";
                Console.Write("hi");
                WindowManager.LastInsatnce.Draw();
            });


        }

        public override void Draw(int ParentX, int ParentY)
        {
            Utilities.ColorPush();

            GraphicalUtils.Fill(ParentX + X, ParentY + Y, ParentX + X + Width, ParentY + Y + Height,
                Style.ButtonBackground);

            Console.BackgroundColor = Style.ButtonBackground;
            Console.ForegroundColor = Style.ButtonText;

            Console.SetCursorPosition(ParentX + X + Width / 2 - Text.Length / 2, ParentY + Y + Height / 2);
            Console.Write(Text);

            Utilities.ColorPop();
        }

        public void OnClick(ClickEventArgs args)
        {
            OnPress(new ButtonPressArgs(this));
        }

        public class ButtonPressArgs : EventArgs
        {
            public Button Pressed;

            public ButtonPressArgs(Button pressed)
            {
                Pressed = pressed;
            }
        }
    }
}
