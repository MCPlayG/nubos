﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NubOS.Windows.Components
{
    public delegate void OnClick(ClickEventArgs args);

    public class ClickEventArgs : EventArgs
    {
        public int X, Y;
    }

    public class Component
    {
        public int X, Y;
        public int Width, Height;

        public Styling Style = DefaultStyles.Light;

        public Window Parent;

        public event OnClick OnPress;

        public Component()
        {
        }

        public virtual void CalculateSize()
        {
        }

        public virtual void Draw(int ParentX, int ParentY)
        {

        }

        public virtual void EnableNotifications()
        {
        }

        public void SendClickEvent(int localX, int localY)
        {
            Console.Write("a");
            ClickEventArgs args = new ClickEventArgs();
            args.X = localX;
            args.Y = localY;
            OnPress(args);
        }

        
    }
}
