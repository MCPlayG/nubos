﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using NubOS.Windows.Components;
using NubOS.Utils;

using System.Drawing;
using Console = Colorful.Console;

namespace NubOS.Windows.Components
{
    public class Label : Component
    {
        public string Text;
        
        public Label(string text)
        {
            Text = text;
        }

        public override void CalculateSize()
        {
            Width = Text.Length;
            Height = 1;
        }

        public override void Draw(int ParentX, int ParentY)
        {
            Utilities.ColorPush();

            Console.BackgroundColor = Style.WindowBackground;
            Console.ForegroundColor = Style.TextColor;
            Console.SetCursorPosition(ParentX + X, ParentY + Y);
            Console.Write(Text);

            Utilities.ColorPop();
        }
    }
}
