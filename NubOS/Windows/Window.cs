﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using NubOS.Windows.Components;
using NubOS.Utils;
using Console = Colorful.Console;
using NubOS.Windows.Components;

namespace NubOS.Windows
{
    public class Window
    {
        public string Title;
        
        public int X, Y;
        public int Width, Height;

        public Styling style;

        public bool Visible;

        public readonly List<Component> Components;

        // Events
        ConsoleLib.ConsoleListener.ConsoleMouseEvent mouseEvent;

        public Window()
        {
            Components = new List<Component>();

            mouseEvent = new ConsoleLib.ConsoleListener.ConsoleMouseEvent(HandleMousePress);
        }

        public void SetVisible(bool flag)
        {
            Visible = flag;

            if (Visible)
            {
                ConsoleLib.ConsoleListener.MouseEvent += mouseEvent;
            }
            else
            {
                ConsoleLib.ConsoleListener.MouseEvent -= mouseEvent;
            }
        }

        public void Draw()
        {
            if (!Visible) return;
            GraphicalUtils.Fill(X, Y, X + Width, Y + Height + 1, style.WindowBackground);

            Console.BackgroundColor = style.TitlebarColor;
            Console.SetCursorPosition(X, Y);
            Console.Write(new string(' ', Width));

            Console.ForegroundColor = style.TitlebarTextColor;
            Console.SetCursorPosition(X + 1, Y);
            Console.Write(Title);

            foreach (Component c in Components)
            {
                c.Style = style;
                c.Draw(X, Y + 1);
            }
        }

        public void HandleMousePress(ConsoleLib.NativeMethods.MOUSE_EVENT_RECORD mouse)
        {
            int localPressX = mouse.dwMousePosition.X - X;
            int localPressY = mouse.dwMousePosition.Y - Y;

            Console.Write(localPressX + " " + localPressY);

            if (localPressX < 0 || localPressY < 0) return;

            foreach (Component c in Components)
            {
                if (Utilities.IsIn(localPressX, localPressY, c.X, c.Y, c.Width, c.Height))
                {
                    c.SendClickEvent(c.X - localPressX, c.Y - localPressY);
                }
            }
        }

        public void AddComponent(Component c)
        {
            c.Parent = this;
            Components.Add(c);
        }
    }
}
