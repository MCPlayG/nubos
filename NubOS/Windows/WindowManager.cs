﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NubOS.Windows
{
    public class WindowManager
    {
        public static WindowManager LastInsatnce;

        public readonly List<Window> Windows;

        public WindowManager()
        {
            Windows = new List<Window>();
        }

        public void Draw()
        {
            foreach(Window w in Windows)
            {
                w.Draw();
            }
        }
    }
}
