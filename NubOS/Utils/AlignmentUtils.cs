﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Console = Colorful.Console;

namespace NubOS.Utils
{
    public class AlignmentUtils
    {
        private AlignmentUtils() { }

        public static int GetCenter(string text)
        {
            return Console.WindowWidth / 2 - text.Length / 2;
        }

        public static void PrintCentered(string text, int top, int xOffset = 0)
        {
            Console.SetCursorPosition(GetCenter(text) + xOffset, top);
            Console.Write(text);
        }
    }
}
