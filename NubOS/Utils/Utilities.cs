﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Drawing;

namespace NubOS.Utils
{
    public class Utilities
    {
        private Utilities() { }

        public static Color FC;
        public static Color BG;

        public static void ColorPush()
        {
            FC = Colorful.Console.ForegroundColor;
            BG = Colorful.Console.BackgroundColor;
        }

        public static void ColorPop()
        {
            Colorful.Console.ForegroundColor = FC;
            Colorful.Console.BackgroundColor = BG;
        }

        public static void Wait(int millis)
        {
            Thread.Sleep(millis);
        }

        public static bool IsIn(int pixelX, int pixelY, int rectangleX, int rectangleY, int width, int height)
        {
            return pixelX >= rectangleX && pixelX <= rectangleX + width &&
                pixelY >= rectangleY && pixelY <= rectangleY + height;
        }
    }
}
