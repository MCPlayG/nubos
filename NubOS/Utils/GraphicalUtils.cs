﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using Console = Colorful.Console;

namespace NubOS.Utils
{
    public class GraphicalUtils
    {
        private GraphicalUtils()
        {
        }

        public static void Fill(int startX, int startY, int endX, int endY, Color fillColor)
        {
            Color oldBg = Console.BackgroundColor;
            Console.BackgroundColor = fillColor;
            for (int y = startY; y < endY; y++)
            {
                Console.SetCursorPosition(startX, y);
                Console.Write(new string(' ', endX - startX));
            }
            Console.BackgroundColor = oldBg;
        }
    }
}
