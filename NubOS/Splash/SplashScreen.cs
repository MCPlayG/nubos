﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NubOS.Utils;

using System.Drawing;
using Console = Colorful.Console;
using System.Threading;

namespace NubOS.Splash
{
    public class SplashScreen
    {
        private SplashScreen()
        {
        }

        public static void StartAndHold(int additionalSeconds)
        {
            Console.BackgroundColor = Color.FromArgb(230, 230, 230);
            Console.Clear();

            Console.ForegroundColor = Color.Black;
            AlignmentUtils.PrintCentered("NubOS", 10, -1);

            Console.ForegroundColor = Color.FromArgb(60, 60, 60);
            AlignmentUtils.PrintCentered("Starting up...", 12);

            Console.ForegroundColor = Color.FromArgb(180, 180, 180);
            AlignmentUtils.PrintCentered(Program.Version + " '" + Program.Codename + "'", 17);
            AlignmentUtils.PrintCentered("(C) Copyright 2018 Mcpg", Console.WindowHeight - 1);

            // loading bar;
            Color[] gradient =
            {
                Color.FromArgb(94, 188, 219),
                Color.FromArgb(67, 179, 216),
                Color.FromArgb(50, 169, 209),
                Color.FromArgb(37, 156, 196),
                Color.FromArgb(31, 154, 196),
                Color.FromArgb(21, 153, 198),
            };

            Console.SetCursorPosition(0, 15);

            for (int i = 0; i < gradient.Length; i++)
            {
                Console.BackgroundColor = gradient[i];
                Console.Write("    "); // 4
            }

            Console.Write(new string(' ', Console.WindowWidth - gradient.Length * 8));

            for (int i = gradient.Length - 1; i >= 0; i--)
            {
                Console.BackgroundColor = gradient[i];
                Console.Write("    "); // 4
            }

            Console.BackgroundColor = gradient[gradient.Length - 1];

            Thread animation = new Thread(() =>
            {
                string[] anim = { "-", "\\", "|", "/" };
                
                Console.ForegroundColor = Color.White;
                while (true)
                {
                    for (int i = 0; i < anim.Length; i++)
                    {
                        Console.SetCursorPosition(AlignmentUtils.GetCenter(" "), 15);
                        Console.Write(anim[i]);
                        Utilities.Wait(500);
                    }
                }
            });

            animation.Start();

            Utilities.Wait(additionalSeconds * 1000);

            animation.Abort();
            Console.ResetColor();
            Console.Clear();
        }
    }
}
