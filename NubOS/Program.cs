﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Console = Colorful.Console;
using System.Drawing;
using NubOS.Utils;
using NubOS.Splash;
using NubOS.Windows;
using NubOS.Windows.Components;

namespace NubOS
{
    class Program
    {
        public const string Version = "0.1.1";
        public const string Codename = "Flying Elephant";

        static void Main(string[] args)
        {
            Console.Title = "NubOS";

            ConsoleLib.ConsoleListener.Start();
            //SplashScreen.StartAndHold(10);

            Console.BackgroundColor = Color.Black;
            Console.Clear();
            

            WindowManager wm = new WindowManager();

            Window w = new Window();
            w.X = 10;
            w.Y = 10;
            w.Width = 45;
            w.Height = 10;
            w.style = DefaultStyles.Light;
            w.Title = "Log in";

            Label lab = new Label("GabeN GabeN GabeN at valvesoftware.com");
            lab.X = 1;
            lab.Y = 0;
            lab.CalculateSize();
            w.AddComponent(lab);

            Button but = new Button();
            but.Text = "elo elo 320";
            but.X = 3;
            but.Y = 3;
            but.Width = 30;
            but.Height = 3;
            but.Style = DefaultStyles.Light;
            w.AddComponent(but);

            w.SetVisible(true);

            wm.Windows.Add(w);
            wm.Draw();

            Console.ReadLine();
        }
    }
}
